option(
	'docs',
	type: 'boolean',
	value: true,
	description : 'Generate valadoc documentation',
)
