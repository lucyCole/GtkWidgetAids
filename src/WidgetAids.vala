namespace GtkWidgetAids {
	namespace HashFuncs {
		public uint uIntHash(
			uint uIntVal
		) {
			return uIntVal;
		}
		public uint intHash(
			int intVal
		) {
			return ( uint) intVal;
		}
		public uint widgetHashFunction(
			Gtk.Widget val
		) {
			return uint.from_pointer( val);
		}
		// public int enumHash(
		// 	enu enumValue
		// ) {
		// 	return ( int) enumValue;
		// }
	}
	public Gee.HashSet< Gtk.Widget> getNewWidgetSet(
		Gtk.Widget[]? widgetArray= null
	) {
		Gee.HashSet< Gtk.Widget> newSet= new Gee.HashSet< Gtk.Widget>(
			HashFuncs.widgetHashFunction
		);
		if( widgetArray!= null) {
			newSet.add_all_array( widgetArray);
		}
		return newSet;
	}
	public class WidgetIterator {
		private Gtk.Widget widget;
		protected Gtk.Widget? currentContained;
		public WidgetIterator(
			Gtk.Widget widget
		) {
			this.widget= widget;
			this.currentContained= null;
		}
		public bool next() {
			Gtk.Widget? nextContained;
			if( this.currentContained== null) {
				nextContained= this.widget.get_first_child();
			}
			else {
				nextContained= this.currentContained.get_next_sibling();
			}
			if( nextContained== null) {
				return false;
			}
			else {
				this.currentContained= nextContained;
				return true;
			}
		}
		public Gtk.Widget @get() {
			return this.currentContained;
		}
	}
	public interface IterableWidget: Gtk.Widget {
		public WidgetIterator iterator() {
			return new WidgetIterator( this);
		}
	}
	/**
	* Behaviour for widget-s which manage selection-s
	*
	* New selection-s can be automatic-ally raised<<BR>>
	* current error is that one can not currently override gtk_widget_pick<<BR>>
	* overriding display order is easy<<BR>>
	* therefore the order of display and picking is the widget order<<BR>>
	* rearanging the widget order during a drag causes issues with the drag<<BR>>
	*/
	public interface WidgetWithSelection: Gtk.Widget, IterableWidget {
		public enum WidgetSelectionAlterationMode {
			REPLACE,
			ADD,
			REMOVE,
			INVERT
		}
		public enum WholeSelectionAlterationMode {
			SELECT,
			UNSELECT,
			INVERT
		}
		public signal void selectionChanged(
			int previousSize,
			int nextSize,
			Gee.HashSet< Gtk.Widget> selection
		);
		public Gee.HashSet< Gtk.Widget> selection {
			get {
				return this.selectionProtected;
			}
		}
		internal abstract Gee.HashSet< Gtk.Widget> selectableWidgets { get; set;}
		protected abstract Gee.HashSet< Gtk.Widget> selectionProtected { get; set;}
		protected void setupSelection() {
			this.selectionProtected= getNewWidgetSet();
		}
		private void setSelectionState(
			Gtk.Widget widget
		) {
			Gtk.StateFlags currentState= widget.get_state_flags();
			if( !( Gtk.StateFlags.SELECTED in currentState)) {
				widget.set_state_flags(
					Gtk.StateFlags.SELECTED,
					false
				);
			}
		}
		private void setUnSelectionState(
			Gtk.Widget widget
		) {
			Gtk.StateFlags currentState= widget.get_state_flags();
			if( Gtk.StateFlags.SELECTED in currentState) {
				widget.set_state_flags(
					currentState- Gtk.StateFlags.SELECTED,
					true
				);
			}
		}
		protected void setNewlyInSelectionBoundState(
			Gtk.Widget widget
		) {
			widget.add_css_class( "inSelectionBound");
		}
		protected void setNewlyNotInSelectionBoundState(
			Gtk.Widget widget
		) {
			widget.remove_css_class( "inSelectionBound");
		}


		/**
		* ( newlySelected, newlyUnSelected) are only guarenteed to be non null when the return value is true
		*/
		public bool alterSetWithWidgets(
			ref Gee.HashSet< Gtk.Widget> set,
			Gee.HashSet< Gtk.Widget> widgets,
			WidgetSelectionAlterationMode alterationMode,
			bool mutateSet,
			out Gee.HashSet< Gtk.Widget>? newlySelected,
			out Gee.HashSet< Gtk.Widget>? newlyUnSelected,
			out int? newLen,
			out int currentLen
		) {
			// CAN EITHER ENFORCE MEMBERSHIP SELECTION IN THE CALLER OR IN HERE, MAYBE OPTIONAL IN HERE
			// widgets.retain_all( this.selectableWidgets);
			currentLen= set.size;
			Gee.HashSet< Gtk.Widget>? newSelection= null;
			// If it is known that no alteration will occur then newSelection is left null
			newLen= null;
			newlySelected= null;
			newlyUnSelected= null;
			bool? altered= null;

			switch( alterationMode) {
				case WidgetSelectionAlterationMode.REPLACE:
					newLen= widgets.size;
					if( currentLen!= newLen) {
						altered= true;
					}
					newSelection= widgets;
					break;
				case WidgetSelectionAlterationMode.ADD:
					if( widgets.size== 0) {
						break;
					}
					widgets.remove_all( set);
					newlySelected= widgets;
					newlyUnSelected= getNewWidgetSet();

					newSelection= getNewWidgetSet();
					newSelection.add_all( set);
					altered= newSelection.add_all( widgets);
					break;
				case WidgetSelectionAlterationMode.REMOVE:
					if( widgets.size== 0) {
						break;
					}
					newlySelected= getNewWidgetSet();
					widgets.retain_all( set);
					newlyUnSelected= widgets;// Those that are in both widgets and current selection
					newSelection= getNewWidgetSet();
					newSelection.add_all( set);
					altered= newSelection.remove_all( widgets);
					break;
				case WidgetSelectionAlterationMode.INVERT:
					// Copy input set
					if( widgets.size== 0) {
						break;
					}
					altered= true;
					newSelection= getNewWidgetSet();
					newSelection.add_all( set);
					// Copy widgets
					Gee.HashSet< Gtk.Widget> unpresent= getNewWidgetSet();
					unpresent.add_all( widgets);
					bool presentAltered= widgets.retain_all( set);
					if( presentAltered== false) {
						// All input-s are present ( `widget-s` unchanged), remove all input-s
						newlySelected= getNewWidgetSet();
						newlyUnSelected= widgets;
						altered= newSelection.remove_all( widgets);
					}
					else {
						// `widget-s` changed
						int presentSize= widgets.size;
						if( presentSize== 0) {
							// No input-s are present , add all input-s
							newlySelected= unpresent;
							newlyUnSelected= getNewWidgetSet();
							newSelection.add_all( unpresent);
						}
						else {
							unpresent.remove_all( widgets);
							newlySelected= unpresent;
							newlyUnSelected= widgets;
							newSelection.remove_all( widgets);
							newSelection.add_all( unpresent);
						}
					}
					newLen= newSelection.size;
					break;
			}

			// Account for possible state-s
			if( newSelection== null) {
				return false;
			}
			if( altered== false) {
				return false;
			}
			if( newlySelected== null) {
				newlySelected= getNewWidgetSet();
				newlySelected.add_all( newSelection);
				newlySelected.remove_all( set);
			}
			if( newlyUnSelected== null) {
				newlyUnSelected= getNewWidgetSet();
				newlyUnSelected.add_all( set);
				newlyUnSelected.remove_all( newSelection);
			}
			if( newLen== null) {
				newLen= newSelection.size;
			}
			if( altered== null) {
				altered= ( newlySelected.size+ newlyUnSelected.size)!= 0;
			}
			if( altered) {
				if( mutateSet) {
					set= newSelection;
				}
				return true;
			} else {
				return false;
			}
		}
		public bool alterSelectionWithWidgets(
			Gee.HashSet< Gtk.Widget> widgets,
			WidgetSelectionAlterationMode alterationMode
		) {
			Gee.HashSet< Gtk.Widget> newlySelected, newlyUnSelected, selection;
			selection= this.selectionProtected;
			int? newLen;
			int currentLen;
			bool altered= alterSetWithWidgets(
				ref selection,
				widgets,
				alterationMode,
				true,
				out newlySelected,
				out newlyUnSelected,
				out newLen,
				out currentLen
			);
			if( altered) {
				this.selectionProtected= selection;
				foreach( Gtk.Widget widget in newlySelected) {
					this.setSelectionState(
						widget
					);
				}
				foreach( Gtk.Widget widget in newlyUnSelected) {
					this.setUnSelectionState( widget);
				}
				this.selectionChanged(
					currentLen,
					newLen,
					this.selectionProtected
				);
			}
			return altered;
		}
		public bool alterSetWholly(
			ref Gee.HashSet< Gtk.Widget> set,
			WholeSelectionAlterationMode alterationMode,
			out Gee.HashSet< Gtk.Widget> newlySelected,
			out Gee.HashSet< Gtk.Widget> newlyUnSelected,
			out int currentLen,
			out int newLen
		) {
			currentLen= set.size;
			newLen= currentLen;
			newlySelected= getNewWidgetSet();
			newlyUnSelected= getNewWidgetSet();
			bool? altered= null;
			switch( alterationMode) {
				case WholeSelectionAlterationMode.SELECT:
					bool changed;
					newLen= 0;
					altered= false;
					foreach( Gtk.Widget contained in this.selectableWidgets) {
						changed= set.add( contained);
						if( changed) {
							newlySelected.add( contained);
							altered= true;
						}
						newLen++;
					}
					break;
				case WholeSelectionAlterationMode.UNSELECT:
					altered= set.size!= 0;
					newlyUnSelected.add_all( set);
					set.clear();
					newLen= 0;
					break;
				case WholeSelectionAlterationMode.INVERT:
					altered= true;
					Gee.HashSet< Gtk.Widget> newSelection= getNewWidgetSet();
					newLen= 0;
					foreach( Gtk.Widget contained in this.selectableWidgets) {
						if( !set.contains( contained)) {
							newSelection.add( contained);
							newLen++;
						} else {
							newlyUnSelected.add( contained);
						}

					}
					newlySelected= newSelection;
					set= newSelection;
					break;
			}
			return altered;
		}
		public bool alterSelectionWholly(
			WholeSelectionAlterationMode alterationMode
		) {
			Gee.HashSet< Gtk.Widget> newlySelected, newlyUnSelected, selection;
			int currentLen, newLen;
			selection= this.selectionProtected;
			bool altered= alterSetWholly(
				ref selection,
				alterationMode,
				out newlySelected,
				out newlyUnSelected,
				out currentLen,
				out newLen
			);
			if( altered) {
				this.selectionProtected= selection;
				foreach( Gtk.Widget widget in newlySelected) {
					this.setSelectionState(
						widget
					);
				}
				foreach( Gtk.Widget widget in newlyUnSelected) {
					this.setUnSelectionState( widget);
				}
				this.selectionChanged(
					currentLen,
					newLen,
					this.selectionProtected
				);
			}
			return altered;
		}
		public static WidgetWithSelection.WidgetSelectionAlterationMode getAlterationModeFromModifiers(
			Gdk.ModifierType modifierState
		) {
			bool ctrlPresent= Gdk.ModifierType.CONTROL_MASK in modifierState;
			bool shiftPresent= Gdk.ModifierType.SHIFT_MASK in modifierState;
			WidgetWithSelection.WidgetSelectionAlterationMode alterationMode;
			if( ctrlPresent) {
				if( shiftPresent) {
					alterationMode= WidgetWithSelection.WidgetSelectionAlterationMode.INVERT;
				} else {
					alterationMode= WidgetWithSelection.WidgetSelectionAlterationMode.REMOVE;
				}
			} else if( shiftPresent) {
				alterationMode= WidgetWithSelection.WidgetSelectionAlterationMode.ADD;
			} else {
				alterationMode= WidgetWithSelection.WidgetSelectionAlterationMode.REPLACE;
			}
			return alterationMode;
		
		}
	}

	public class Bin: Gtk.Widget {
		private class uint sizeResetAnimationDuration= 100;
		class construct {
			set_css_name( "Bin");
		}
		public enum Sizing {
			OWN,
			CONTAINED
		}
		public delegate bool AnimateChangeCallback(
			double value
		);
		private struct SizeResetAnimationData {
			public double widthStart;
			public double widthDifference;
			public double heightStart;
			public double heightDifference;
			public int width;
			public int height;
			public Adw.TimedAnimation animation;
			public AnimateChangeCallback? beforeFunc;
			public AnimateChangeCallback? afterFunc;
		}
		private SizeResetAnimationData? sizeResetAnimationData= null;
		private Gtk.Widget? _contained= null;
		public Gtk.Requisition ownSize { get; set; default= Gtk.Requisition();}
		private Sizing _sizingActual= Sizing.OWN; // Default _contained is null and default _sizing is CONTAINED so default sizingActual is OWN
		public Sizing sizingActual { get { return this._sizingActual;}}
		private Sizing _sizing= Sizing.CONTAINED;
		public Sizing sizing {
			get {
				return this._sizing;
			}
			set {
				this._sizing= value;
				this.calculateSizingActual();
			}
		}
		public Gtk.Widget? contained {
			get {
				return this._contained;
			}
			set {
				if( value== null) {
					if( this._contained!= null) {
						this._contained.unparent();
					}
				}
				else {
					value.set_parent( this);
				}
				this._contained= value;
				this.calculateSizingActual();
			}
		}
		public Bin() {
			this.set_overflow( Gtk.Overflow.HIDDEN);
		}
		private void calculateSizingActual() {
			Sizing? newSizing= null;
			switch( this._sizing) {
				case( Sizing.CONTAINED):
					if( this.contained!= null) {
						newSizing= Sizing.CONTAINED;
					} else {
						newSizing= Sizing.OWN;
					}
					break;
				case( Sizing.OWN):
					newSizing= Sizing.OWN;
					break;
			}
			if( this._sizingActual!= newSizing) {
				this.queue_resize();
			}
			this._sizingActual= newSizing;
		}
		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			if( this.sizeResetAnimationData!= null) {
				int ownDimension= ( orientation== Gtk.Orientation.HORIZONTAL)? this.sizeResetAnimationData.width: this.sizeResetAnimationData.height;
				minimum= ownDimension;
				natural= ownDimension;
				minimum_baseline= natural_baseline= -1;
			} else {
				switch( this._sizingActual) {
					case( Sizing.OWN):
							int ownDimension= ( orientation== Gtk.Orientation.HORIZONTAL)? this.ownSize.width: this.ownSize.height;
							minimum= ownDimension;
							natural= ownDimension;
							minimum_baseline= natural_baseline= -1;
						break;
					case( Sizing.CONTAINED):
						this.contained.measure(
							orientation,
							for_size,
							out minimum,
							out natural,
							out minimum_baseline,
							out natural_baseline
						);
						break;
				}
			}
		}
		protected void allocateContained(
			int width,
			int height,
			int baseline
		) {
			if( ( !( this._sizingActual== Sizing.CONTAINED))|| this.sizeResetAnimationData!= null) {
				if( this.contained!= null) {
					// Assign the max of the bin and the widget~s minimum
					int minWidth, minHeight, dud;
					switch( this.contained.get_request_mode()) {
						case( Gtk.SizeRequestMode.CONSTANT_SIZE):
							this.contained.measure( Gtk.Orientation.HORIZONTAL, -1, out minWidth, out dud, out dud, out dud);
							this.contained.measure( Gtk.Orientation.VERTICAL, -1, out minHeight, out dud, out dud, out dud);
							break;
						case( Gtk.SizeRequestMode.WIDTH_FOR_HEIGHT):
							this.contained.measure( Gtk.Orientation.VERTICAL, -1, out minHeight, out dud, out dud, out dud);
							this.contained.measure( Gtk.Orientation.HORIZONTAL, minHeight, out minWidth, out dud, out dud, out dud);
							break;
						case( Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH):
							this.contained.measure( Gtk.Orientation.HORIZONTAL, -1, out minWidth, out dud, out dud, out dud);
							this.contained.measure( Gtk.Orientation.VERTICAL, minWidth, out minHeight, out dud, out dud, out dud);
							break;
						default:
							minWidth= width;
							minHeight= height;
							break;
					}
					this.contained.allocate(
						width> minWidth? width: minWidth,
						height> minHeight? height: minHeight,
						baseline,
						null
					);
				}
			} else {
				this.contained.allocate(
					width,
					height,
					baseline,
					null
				);
			}
		}
		public override void size_allocate(
			int width,
			int height,
			int baseline
		) {
			this.allocateContained(
				width,
				height,
				baseline
			);
		}
		public override Gtk.SizeRequestMode get_request_mode() {
			Gtk.SizeRequestMode? requestMode= null;
			switch( this._sizingActual) {
				case( Sizing.OWN):
					requestMode= Gtk.SizeRequestMode.CONSTANT_SIZE;
					break;
				case( Sizing.CONTAINED):
					requestMode= this.contained.get_request_mode();
					break;
			}
			return requestMode;
		}
		public bool animateChange(
			Sizing? sizing= null,
			Gtk.Requisition? ownSize= null,
			AnimateChangeCallback? beforeFunc= null,
			AnimateChangeCallback? afterFunc= null
		) {
			bool change= false, animating= false;
			Gtk.Requisition startReq= Gtk.Requisition();
			Gtk.Requisition endReq= Gtk.Requisition();
			if( sizing!= null) {
				if( sizing!= this.sizing) {
					// Sizing will therefore change, even if actual does not
					if( this.sizing== Sizing.OWN) {
						if( this.contained!= null) {
							// Then changing from own to contained prefered so animate
							animating= true;
							// Ignore any change in ownSize for animation purposes
							startReq= this._ownSize.copy();
							Gtk.Requisition dud;
							this.contained.get_preferred_size(
								out dud,
								out endReq
							);
						} else {
							// Otherwise changing from own to contained ( own actual)
							// Only animate if change in ownSize
							if( ownSize!= null) {
								animating= true;
								startReq= this._ownSize.copy();
								endReq= ownSize.copy();
							}
						}
					} else {
						// Then changing from contained to own
						bool ownActualToSameOwn= false;
						if( this.contained!= null) {
							// Start is contained
							startReq.width= this.contained.get_width();
							startReq.height= this.contained.get_height();
						} else {
							// Start is existing ownSize
							if( ownSize== null) {
								ownActualToSameOwn= true;
							} else {
								startReq= this.ownSize.copy();
							}
						}
						if( !ownActualToSameOwn) {
							animating= true;
							// Take into account any ownSize change
							if( ownSize!= null) {
								endReq= ownSize.copy();
							} else {
								endReq= this.ownSize.copy();
							}
						}
					}
				}
			} else {
				if( ownSize!= null) {
					if( this.sizingActual== Sizing.OWN) {
						animating= true;
						startReq= this._ownSize.copy();
						endReq= ownSize.copy();
					}
				}
			}
			if( sizing!= null) {
				change= true;
				this.sizing= sizing;
			}
			if( ownSize!= null) {
				change= true;
				this.ownSize= ownSize;
			}
			if( animating) {
				Adw.CallbackAnimationTarget callbackTarget= new Adw.CallbackAnimationTarget(
					this.animateChangeCallback
				);
				Adw.TimedAnimation animation= new Adw.TimedAnimation(
					this,
					0.0,
					1.0,
					this.sizeResetAnimationDuration,
					callbackTarget
				);
				animation.done.connect(
					this.animateChangeDone
				);
				this.sizeResetAnimationData= SizeResetAnimationData();
				this.sizeResetAnimationData.widthStart= ( double) startReq.width;
				this.sizeResetAnimationData.widthDifference= ( double) ( endReq.width- startReq.width);
				this.sizeResetAnimationData.heightStart= ( double) startReq.height;
				this.sizeResetAnimationData.heightDifference= ( double) ( endReq.height- startReq.height);
				this.sizeResetAnimationData.width= startReq.width;
				this.sizeResetAnimationData.height= startReq.height;
				this.sizeResetAnimationData.animation= animation;
				this.sizeResetAnimationData.beforeFunc= beforeFunc;
				this.sizeResetAnimationData.afterFunc= afterFunc;
				animation.play();
			}

			return change;
		}
		private void animateChangeCallback(
			double value
		) {
			if( this.sizeResetAnimationData.beforeFunc!= null) {
				if( !this.sizeResetAnimationData.beforeFunc(
					value
				)) {
					this.sizeResetAnimationData.animation.skip();
					return;
				}
			}
			this.sizeResetAnimationData.width= ( int) ( this.sizeResetAnimationData.widthStart+ ( this.sizeResetAnimationData.widthDifference* value));
			this.sizeResetAnimationData.height= ( int) ( this.sizeResetAnimationData.heightStart+ ( this.sizeResetAnimationData.heightDifference* value));
			this.queue_resize();
			if( this.sizeResetAnimationData.afterFunc!= null) {
				if( !this.sizeResetAnimationData.afterFunc(
					value
				)) {
					this.sizeResetAnimationData.animation.skip();
					return;
				}
			}
		}
		private void animateChangeDone(
			Adw.Animation animation
		) {
			this.sizeResetAnimationData= null;
		}
	}
	
}