( define-module
	( GtkWidgetAids)

	#:use-module ( guix git-download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build utils)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module (
		( lucyChannel packages shiny-gnome gobject-introspection)
		#:select (
			( gobject-introspection . lucyGI)
		)
	)
	#:use-module (
		( lucyChannel packages shiny-gnome glib)
		#:select (
			( glib . lucyGLib)
		)
	)
	#:use-module (
		( lucyChannel packages shiny-gnome gtk)
		#:select (
			( gtk . lucyGtk)
		)
	)
	#:use-module (
		( lucyChannel packages shiny-gnome gnome)
		#:select (
			( vala . lucyVala)
		)
	)
	#:use-module (
		( gnu packages gnome)
		#:select (
			libgee
			libadwaita
		)
	)
	#:use-module ( gnu packages pkg-config)

	#:export (
		GtkWidgetAids
	)
)


; some of these maybe implicit for meson build

; vala, with valadoc
; gobject-introspection
; pkg-config

; glib2
; gobject2     think this is not provided by some other thing
; gtk4
; gee0.8
; libadwaita1


( define
	GtkWidgetAids
	( let
		( 
			( projectVersion "1.0")
			( revision "0")
			( commit "4e0714fb74775c8e6315573c4e713eb3cd0e9218")
		)
		( package
			( name "GtkWidgetAids")
			( version
				( git-version
					projectVersion
					revision
					commit
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://gitlab.gnome.org/lucyCole/GtkWidgetAids.git")
							( recursive? #false)
						)
					)
					( sha256
						( base32 "0pw6yqdyna3cnsps9fykmmdj358lby2cfs3305hz06spa610yij5")
					)
				)
			)
			( build-system meson-build-system)
			( inputs
				( list
					lucyVala
					pkg-config
				)
			)
			( propagated-inputs
				( list
					lucyGI
					lucyGLib
					lucyGtk
					libgee
					libadwaita
				)
			)
			( synopsis "Gtk help-er function-s used by me: ( the packager!)")
			( description
				"Contiains, bin, selection interface, some other stuff"
			)
			( license license.lgpl2.1+)
			( home-page "https://gitlab.gnome.org/lucyCole/GtkWidgetAids")

		)
	)
)
GtkWidgetAids
